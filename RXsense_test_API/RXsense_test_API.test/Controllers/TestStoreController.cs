﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RXsense_test_API.Models;
using RXsense_test_API.Repository;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RXsense_test_API.Controllers
{
    [TestClass]
    public class TestStoreController
    {

        [TestMethod]
        public void GetstoreReturnOk()
        {
            // Arrange
            var testStore = new List<Storeviewmodel>();
            testStore.Add(new Storeviewmodel { Name = "Msstore", country_id = 1, countryName = "India" });
            PagingParameterModel pagingparametermodel = new PagingParameterModel();
            var mockRepository = new Mock<IstoreRepository>();
            mockRepository.Setup(x => x.getStore(1, pagingparametermodel)).Returns(testStore);
            var controller = new StoreController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            //Act
            var actionResult = controller.Get(1, pagingparametermodel);
            //Assert
            Assert.AreEqual(HttpStatusCode.OK, actionResult.StatusCode);
        }
        [TestMethod]
        public void GetstoreReturnNotfound()
        {
            // Arrange
            var testStore = new List<Storeviewmodel>();
            testStore.Add(new Storeviewmodel { Name = "Msstore", country_id = 1, countryName = "India" });
            PagingParameterModel pagingparametermodel = new PagingParameterModel();
            var mockRepository = new Mock<IstoreRepository>();
            mockRepository.Setup(x => x.getStore(-1, pagingparametermodel)).Returns(testStore);
            var controller = new StoreController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            //Act
            var actionResult = controller.Get(-1, pagingparametermodel);
            //Assert
            Assert.AreEqual(HttpStatusCode.NotFound, actionResult.StatusCode);
        }
    }
}
