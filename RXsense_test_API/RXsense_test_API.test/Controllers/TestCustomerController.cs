﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RXsense_test_API.Models;
using RXsense_test_API.Myconnection;
using RXsense_test_API.Repository;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RXsense_test_API.Controllers
{
    [TestClass]
    public class TestCustomerController
    {
        [TestMethod]
        public void GetAllCustomer()
        {
            // Arrange
            PagingParameterModel pagingparametermodel = new PagingParameterModel();
            var testCustomer = new List<Customerviewmodel>();
            testCustomer.Add(new Customerviewmodel { Id = 1, Name = "Minu", store_id = 1, Address = "Mansa", Age = "24", storeName = "Msstore" });
            var mockRepository = new Mock<ICustomerRepository>();
            mockRepository.Setup(x => x.Get(pagingparametermodel, "Minu")).Returns(testCustomer);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            //Act
            var actionResult = controller.Get(pagingparametermodel, "Minu");
            //Assert
            Assert.AreEqual(HttpStatusCode.OK, actionResult.StatusCode);
        }
        [TestMethod]
        public void DeleteReturnsOk()
        {
            // Arrange
            var mockRepository = new Mock<ICustomerRepository>();
            mockRepository.Setup(x => x.Delete(1)).Returns(1);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();

            // Act
            var actionResult = controller.Delete(1);
            Assert.AreEqual(HttpStatusCode.OK, actionResult.StatusCode);
        }

        [TestMethod]
        public void DeleteReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<ICustomerRepository>();
            mockRepository.Setup(x => x.Delete(1)).Returns(0);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            // Act
            var actionResult = controller.Delete(-1);
            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, actionResult.StatusCode);
        }

        [TestMethod]
        public void PostReturnsNotFound()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var actionResult = controller.PostCustomer(null);
            Assert.AreEqual(HttpStatusCode.NotFound, actionResult.StatusCode);
        }

        [TestMethod]
        public void PostReturnsBadrequest()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var testCustomer = GetTestCustomer();
            mockRepository.Setup(x => x.AddOrEdit(testCustomer[0])).Returns(0);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var actionResult = controller.PostCustomer(testCustomer[0]);
            Assert.AreEqual(HttpStatusCode.NotFound, actionResult.StatusCode);
        }
        [TestMethod]
        public void PostReturnsOk()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var testCustomer = GetTestCustomer();
            mockRepository.Setup(x => x.AddOrEdit(testCustomer[0])).Returns(1);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var actionResult = controller.PostCustomer(testCustomer[0]);
            Assert.AreEqual(HttpStatusCode.OK, actionResult.StatusCode);
        }
        [TestMethod]
        public void Postmodelnotvalid()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var testCustomer = GetTestCustomer();
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            controller.Validate(testCustomer[1]);
            var actionResult = controller.PostCustomer(testCustomer[1]);
            Assert.AreEqual(HttpStatusCode.BadRequest, actionResult.StatusCode);
        }

        [TestMethod]
        public void PutReturnsNotFound()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var actionResult = controller.PutCustomer(null);
            Assert.AreEqual(HttpStatusCode.NotFound, actionResult.StatusCode);
        }

        [TestMethod]
        public void PutReturnsBadrequest()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var testCustomer = GetTestCustomer();
            mockRepository.Setup(x => x.AddOrEdit(testCustomer[0])).Returns(0);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var actionResult = controller.PutCustomer(testCustomer[0]);
            Assert.AreEqual(HttpStatusCode.NotFound, actionResult.StatusCode);
        }
        [TestMethod]
        public void PutModelNotvalid()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var testCustomer = GetTestCustomer();
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            controller.Validate(testCustomer[1]);
            var actionResult = controller.PutCustomer(testCustomer[1]);
            Assert.AreEqual(HttpStatusCode.BadRequest, actionResult.StatusCode);
        }
        [TestMethod]
        public void PutOkreturnforupdate()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var testCustomer = GetTestCustomer();
            mockRepository.Setup(x => x.AddOrEdit(testCustomer[0])).Returns(1);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var actionResult = controller.PutCustomer(testCustomer[0]);
            Assert.AreEqual(HttpStatusCode.OK, actionResult.StatusCode);
        }
        [TestMethod]
        public void PutReturnsOk()
        {
            var mockRepository = new Mock<ICustomerRepository>();
            var testCustomer = GetTestCustomer();
            mockRepository.Setup(x => x.AddOrEdit(testCustomer[0])).Returns(2);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var actionResult = controller.PutCustomer(testCustomer[0]);
            Assert.AreEqual(HttpStatusCode.OK, actionResult.StatusCode);
        }
        private List<Customer> GetTestCustomer()
        {
            var testCustomer = new List<Customer>();
            testCustomer.Add(new Customer { Id = 1, Name = "Minu", store_id = 1, Address = "Mansa", Age = "24" });
            testCustomer.Add(new Customer { Id = 1, Name = null, store_id = 1, Address = null, Age = "24" });
            return testCustomer;
        }
        [TestMethod]
        public void GetCustomerbyidOk()
        {
            // Arrange
            var testCustomer = new List<Customerviewmodel>();
            testCustomer.Add(new Customerviewmodel { Id = 1, Name = "Minu", store_id = 1, Address = "Mansa", Age = "24", storeName = "Msstore" });
            PagingParameterModel pagingparametermodel = new PagingParameterModel();
            var mockRepository = new Mock<ICustomerRepository>();
            mockRepository.Setup(x => x.Get(1, pagingparametermodel)).Returns(testCustomer);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            //Act
            var actionResult = controller.GetCustomer(1, pagingparametermodel);
            //Assert
            Assert.AreEqual(HttpStatusCode.OK, actionResult.StatusCode);
        }
        [TestMethod]
        public void GetCustomerbyidNotfound()
        {
            // Arrange
            var testCustomer = new List<Customerviewmodel>();
            testCustomer.Add(new Customerviewmodel { Id = 1, Name = "Minu", store_id = 1, Address = "Mansa", Age = "24", storeName = "Msstore" });
            PagingParameterModel pagingparametermodel = new PagingParameterModel();
            var mockRepository = new Mock<ICustomerRepository>();
            mockRepository.Setup(x => x.Get(-1, pagingparametermodel)).Returns(testCustomer);
            var controller = new CustomerController(mockRepository.Object);
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            //Act
            var actionResult = controller.GetCustomer(-1, pagingparametermodel);
            //Assert
            Assert.AreEqual(HttpStatusCode.NotFound, actionResult.StatusCode);
        }
    }
}
