﻿using RXsense_test_API.Myconnection;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace RXsense_test_API.Contex
{
    public class RxsenseContext: DbContext
    {
        public RxsenseContext() : base("name=RxsenseTestEntities")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Store> Stores { get; set; }
        public virtual DbSet<Country> Countries { get; set; }

    }
}