﻿
using RXsense_test_API.Contex;
using RXsense_test_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RXsense_test_API.Repository
{
    public class StoreRepository : IstoreRepository
    {
        private readonly RxsenseContext _context;
        public StoreRepository()
        {
            _context = new RxsenseContext();
        }
        public StoreRepository(RxsenseContext context)
        {
            _context = context;
        }
        public IEnumerable<Storeviewmodel> getStore(int countryId, PagingParameterModel pagingparametermodel)
        {
            try
            {
                var storeviewmodel = from s in _context.Stores
                                     join cnt in _context.Countries
                                     on s.country_id equals cnt.Id
                                     where s.country_id == countryId
                                     orderby s.id
                                     select new Storeviewmodel
                                     {
                                         Name = s.Name,
                                         country_id = s.country_id,
                                         countryName = cnt.Name
                                     };
                return storeviewmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}