﻿using RXsense_test_API.Models;
using RXsense_test_API.Myconnection;
using System.Collections.Generic;

namespace RXsense_test_API.Repository
{
    public interface ICustomerRepository
    {
        IEnumerable<Customerviewmodel> Get(PagingParameterModel pagingparametermodel, string name = null);
        IEnumerable<Customerviewmodel> Get(int id, PagingParameterModel pagingparametermodel);
        int Delete(int id);
        int AddOrEdit(Customer Model);

    }
}
