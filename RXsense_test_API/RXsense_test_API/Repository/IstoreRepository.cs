﻿using RXsense_test_API.Areas.HelpPage.Models;
using RXsense_test_API.Models;
using System.Collections.Generic;

namespace RXsense_test_API.Repository
{
    public interface IstoreRepository
    {
        IEnumerable<Storeviewmodel> getStore(int countryId, PagingParameterModel pagingparametermodel);
    }
}
