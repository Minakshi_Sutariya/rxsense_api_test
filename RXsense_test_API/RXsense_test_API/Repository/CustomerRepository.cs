﻿using RXsense_test_API.Contex;
using RXsense_test_API.Models;
using RXsense_test_API.Myconnection;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace RXsense_test_API.Repository
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly RxsenseContext _context;
        public CustomerRepository()
        {
            _context = new RxsenseContext();
        }
        public CustomerRepository(RxsenseContext context)
        {
            _context = context;
        }
        public IEnumerable<Customerviewmodel> Get(PagingParameterModel pagingparametermodel, string name = null)
        {
            try
            {
                var customerviewmodel = from cus in _context.Customers
                                        join s in _context.Stores
                                        on cus.store_id equals s.id
                                        orderby cus.Id
                                        select new Customerviewmodel
                                        {
                                            Id = cus.Id,
                                            Name = cus.Name,
                                            store_id = cus.store_id,
                                            Age = cus.Age,
                                            Address = cus.Address,
                                            storeName = s.Name
                                        };
                if (name != null)
                {
                    customerviewmodel = customerviewmodel.Where(a => a.Name == name).AsQueryable();
                }
                return customerviewmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IEnumerable<Customerviewmodel> Get(int id, PagingParameterModel pagingparametermodel)
        {
            try
            {
                var customerviewmodel = from cus in _context.Customers
                                        join s in _context.Stores
                                        on cus.store_id equals s.id
                                        where cus.store_id == id
                                        orderby cus.Id
                                        select new Customerviewmodel
                                        {
                                            Id = cus.Id,
                                            Name = cus.Name,
                                            store_id = cus.store_id,
                                            Age = cus.Age,
                                            Address = cus.Address,
                                            storeName = s.Name
                                        };
                return customerviewmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int Delete(int id)
        {
            try
            {
                var cus = _context.Customers.Find(id);
                if (cus != null)
                {
                    _context.Customers.Remove(cus);
                    _context.SaveChanges();
                    return 1;
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int AddOrEdit(Customer Model)
        {
            try
            {
                if (_context != null)
                {
                    if (Model.Id == 0)
                    {
                        _context.Customers.Add(Model);
                        _context.SaveChanges();
                        return 1;
                    }
                    if (Model.Id > 0)
                    {
                        var cus = _context.Customers.Find(Model.Id);
                        if (cus != null)
                        {
                            _context.Entry(cus).State = EntityState.Detached;
                            _context.Entry(Model).State = EntityState.Modified;
                            _context.SaveChanges();
                            return 2;
                        }
                        return 0;
                    }
                }
                return 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
