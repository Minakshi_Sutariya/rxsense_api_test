﻿using RXsense_test_API.Contex;
using RXsense_test_API.Models;
using RXsense_test_API.Myconnection;
using RXsense_test_API.Repository;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RXsense_test_API.Controllers
{
    public class CustomerController : ApiController
    {
        private readonly ICustomerRepository cus_Repo;
        public CustomerController()
        {
            cus_Repo = new CustomerRepository(new RxsenseContext());
        }
        public CustomerController(ICustomerRepository e)
        {
            cus_Repo = e;
        }
        /// <summary>
        /// list of customer.serch the customer by passing  the name as param
        /// </summary>
        /// <param name="pagingparametermodel"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public HttpResponseMessage Get([FromUri]PagingParameterModel pagingparametermodel, string name = null)
        {
            var customerviewmodel = cus_Repo.Get(pagingparametermodel, name);
            var response = new
            {
                records = customerviewmodel.Skip((pagingparametermodel.pageNumber - 1) * pagingparametermodel.pageSize).Take(pagingparametermodel.pageSize).ToList(),
                totalCount = customerviewmodel.Count()
            };
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        /// <summary>
        /// Find the customer by passing  the store id as id param
        /// </summary>
        /// <param name="id"></param>
        /// <param name="pagingparametermodel"></param>
        /// <returns></returns>
        public HttpResponseMessage GetCustomer(int id, [FromUri]PagingParameterModel pagingparametermodel)
        {
            if (id <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, id);
            }
            var customerviewmodel = cus_Repo.Get(id, pagingparametermodel);
            var response = new
            {
                records = customerviewmodel.Skip((pagingparametermodel.pageNumber - 1) * pagingparametermodel.pageSize).Take(pagingparametermodel.pageSize).ToList(),
                totalCount = customerviewmodel.Count()
            };
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
        /// <summary>
        /// Delete the customer
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
            MessageResponse messageResponse = new MessageResponse();
            if (id <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, id);
            }
            int result = cus_Repo.Delete(id);
            if (result == 1)
            {
                messageResponse.statuscode = HttpStatusCode.OK;
                messageResponse.message = "Customer deleted.";
                return Request.CreateResponse(HttpStatusCode.OK, messageResponse);
            }
            else if (result == 0)
            {
                messageResponse.statuscode = HttpStatusCode.NotFound;
                messageResponse.message = "Customer not found.";
                return Request.CreateResponse(HttpStatusCode.NotFound, messageResponse);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest, messageResponse);
        }
        /// <summary>
        /// Post customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public HttpResponseMessage PostCustomer(Customer customer)
        {
            MessageResponse messageResponse = new MessageResponse();
            if (customer != null)
            {
                if (ModelState.IsValid)
                {
                    int post = cus_Repo.AddOrEdit(customer);
                    if (post == 1)
                    {
                        messageResponse.statuscode = HttpStatusCode.OK;
                        messageResponse.message = "Customer added successfully.";
                        return Request.CreateResponse(HttpStatusCode.OK, messageResponse);
                    }
                    else if (post == 0)
                    {
                        messageResponse.statuscode = HttpStatusCode.BadRequest;
                        messageResponse.message = "Customer not found.";
                        return Request.CreateResponse(HttpStatusCode.NotFound, messageResponse);
                    }
                    else if (post == 2)
                    {
                        messageResponse.statuscode = HttpStatusCode.OK;
                        messageResponse.message = "Customer updated successfully.";
                        return Request.CreateResponse(HttpStatusCode.OK, messageResponse);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
        /// <summary>
        /// Update Customer
        /// </summary>
        /// <param name="customer"></param>
        /// <returns></returns>
        public HttpResponseMessage PutCustomer(Customer customer)
        {
            MessageResponse messageResponse = new MessageResponse();
            if (customer != null)
            {
                if (ModelState.IsValid)
                {
                    int post = cus_Repo.AddOrEdit(customer);
                    if (post == 1)
                    {
                        messageResponse.statuscode = HttpStatusCode.OK;
                        messageResponse.message = "Customer added successfully.";
                        return Request.CreateResponse(HttpStatusCode.OK, messageResponse);
                    }
                    else if (post == 2)
                    {
                        messageResponse.statuscode = HttpStatusCode.OK;
                        messageResponse.message = "Customer updated successfully.";
                        return Request.CreateResponse(HttpStatusCode.OK, messageResponse);
                    }
                    else if (post == 0)
                    {
                        messageResponse.statuscode = HttpStatusCode.BadRequest;
                        messageResponse.message = "Customer not found.";
                        return Request.CreateResponse(HttpStatusCode.NotFound, messageResponse);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState.Values);
            }
            return Request.CreateResponse(HttpStatusCode.NotFound);
        }
    }
}
