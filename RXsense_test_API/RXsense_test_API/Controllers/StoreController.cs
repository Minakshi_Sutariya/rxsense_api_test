﻿using RXsense_test_API.Contex;
using RXsense_test_API.Models;
using RXsense_test_API.Repository;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RXsense_test_API.Controllers
{
    public class StoreController : ApiController
    {
        private readonly IstoreRepository store_Repo;
        public StoreController()
        {
            store_Repo = new StoreRepository(new RxsenseContext());
        }
        public StoreController(IstoreRepository s)
        {
            store_Repo = s;
        }

        public HttpResponseMessage Get(int id, [FromUri]PagingParameterModel pagingparametermodel)
        {
            if (id <= 0)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound, id);
            }
            var storeviewmodel = store_Repo.getStore(id, pagingparametermodel);
            var response = new
            {
                records = storeviewmodel.Skip((pagingparametermodel.pageNumber - 1) * pagingparametermodel.pageSize).Take(pagingparametermodel.pageSize).ToList(),
                totalCount = storeviewmodel.Count()
            };
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }
    }
}
