﻿using System;

namespace RXsense_test_API.Models
{
    public class Customerviewmodel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Nullable<int> country_id { get; set; }
        public Nullable<int> store_id { get; set; }
        public string Address { get; set; }
        public string Age { get; set; }

        public string storeName { get; set; }
    }
}