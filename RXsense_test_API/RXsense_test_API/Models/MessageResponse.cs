﻿using RXsense_test_API.Myconnection;
using System.Collections.Generic;
using System.Net;

namespace RXsense_test_API.Models
{
    public class MessageResponse
    {
        public string message { get; set; }
        public HttpStatusCode statuscode { get; set; }
        public IEnumerable<Customer> records { get; set; }
    }
}